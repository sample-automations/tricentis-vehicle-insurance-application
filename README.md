# Tricentis - Vehicle Insurance Application

Automated tests for the sample application presented in: http://sampleapp.tricentis.com/101/app.php
These tests was realized using Selenium WebDriver (Java) and Cucumber.

These tests include the following steps:

1- Access : http://sampleapp.tricentis.com/101/app.php;
2- Fill in the form on tab "Enter Vehicle Data" and press Next;
3- Fill in the form on tab "Enter Insurant Data" and press Next;
4- Fill in the form on tab "Enter Product Data" and press Next;
5- Fill in the form on tab "Select Price Option" and press Next;
6- Fill in the form on tab "Send Quote" and press Send;
7- Check success message displayed on screen

Important Notes:

*The default Driver set is the htmlunitdriver, which does not have a graphical interface.
*The default Drivers configured for GUI are only executable on mac operating system;
*Executable drivers were included for the windows operating system, but must be set on BrowserFactory.class
